//Upvote/Downvote functionality:
function downvote(pid, currcount, user, owner){
	post = document.getElementById(pid);
	$.ajax({
		type: "POST",
		url: "downvote.php",
		data: {pid: pid, currcount: currcount, username: user, author: owner},
		success: function(data) {
			post.innerHTML = data;
			$('.downvote-'+pid).attr('disabled', true);
			$('.upvote-'+pid).attr('disabled', false);
		}
	}).fail(function( jqXHR, textStatus ) {
  		alert( "Request failed: " + textStatus );
	});
}
function upvote(pid, currcount, user, owner){
	post = document.getElementById(pid);
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "upvote.php",
		data: {pid: pid, currcount: currcount, username: user, author: owner},
		success: function(data){
			post.innerHTML = data;
			$('.upvote-'+pid).attr('disabled', true);	
			$('.downvote-'+pid).attr('disabled', false);
		}
	});
}
//Comment page Javascript
function openReply(){
	$('.reply').css("display", "block");
	$('.open-reply').css("display", "none");
	$('.close-reply').css("display", "block");
}
function closeReply(){
	$('.reply').css("display", "none");
	$('.open-reply').css("display", "block");
	$('.close-reply').css("display", "none");
}
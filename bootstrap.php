<?php
/*
 _____  _    _ _____ _ _   
|  __ \| |  | |  __ (_) |  
| |__) | |__| | |__) || |_ 
|  ___/|  __  |  ___/ | __|
| |    | |  | | |   | | |_ 
|_|    |_|  |_|_|   |_|\__|
                                                      
 ____              _       _                                   
|  _ \            | |     | |                                  
| |_) | ___   ___ | |_ ___| |_ _ __ __ _ _ __  _ __   ___ _ __ 
|  _ < / _ \ / _ \| __/ __| __| '__/ _` | '_ \| '_ \ / _ \ '__|
| |_) | (_) | (_) | |_\__ \ |_| | | (_| | |_) | |_) |  __/ |   
|____/ \___/ \___/ \__|___/\__|_|  \__,_| .__/| .__/ \___|_|   
                                        | |   | |              
                                        |_|   |_|              

This application initiates PHPit by connecting to the database specified in tools/dbcon.php and initiating global varibles.
It also loads the selected page.
*/

#some warnings are being particularly stupid, so we only want to see errors

#Uncomment to disable "debugging mode" (it'll hide errors for a production build).
#error_reporting(0);



date_default_timezone_set('UTC');
session_start();

#include classes
include('classes/dbcon.php');
#initiate classes
$dbcon = new dbcon();

#connect to the database
$dbcon->conn();
    

    #Comment section db stuff
    $cmid = htmlspecialchars($_GET['c'], ENT_QUOTES );
    $cmnt = mysql_query("SELECT * FROM comments WHERE pid = '$cmid' ORDER BY id DESC") or die(mysql_error());

    #main links db stuff
    $data = mysql_query("SELECT * FROM links ORDER BY id DESC") or die(mysql_error());


    #logout the user
    if(isset($_GET['lgt'])){
        session_destroy();
        header("Location: /");
        die();
    }
    #confirm email address
    if(isset($_GET['confirm'])){
        $user = htmlspecialchars($_GET['user'], ENT_QUOTES);
        $q = "UPDATE users SET emailconfirmed = '1' WHERE username = '$user'";
        mysql_query($q) or die(mysql_error());
    }

    #Begin rendering the page
    include('page/header.php');

    #Choose what page to render (Home, Submit, Comments ETC...)
    if(isset($_GET['s'])){
    	include('page/submit.php');
    }
    else if(isset($_GET['comments'])){
    	include('page/comments.php');
    }
    else if(isset($_GET['su'])){
        include('page/signup.php');
    }
    else if(isset($_GET['profile'])){
        include('page/profile.php');
    }
    else {
        include('page/content.php');
    }

    #Finish rendering the page
    include('page/foot.php');
?>
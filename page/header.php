<?php 
if(isset($_GET['s'])){
        $title = "Submit";
    }
    else if(isset($_GET['comments'])){
        $title = "Comments";
    }
    else if(isset($_GET['su'])){
        $title = "Sign Up";
    }
    else {
        $title = "Home";
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php print $title; ?> | PHPit: Simple PHP link aggregation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-responive.min.css" rel="stylesheet">
		<style>
      	body {
        	padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
     	 }
  	  	</style>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/script.js"></script>
	</head>
	<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">PHPit</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/?s">Submit</a></li>
          </ul>
          <div class ="navbar-right">
            <?php if(!isset($_SESSION['username'])) { ?>
             <form class="navbar-form" action = "page/login.php" method = "POST">
              <div class="form-group">
                <input type="text" placeholder="Username" name="username" class="form-control">
              </div>
              <div class="form-group">
                <input type="password" name="password" placeholder="Password" class="form-control">
              </div>
              <button type="submit" class="btn btn-primary">Sign in</button>
              <a href="/?su" class="btn btn-info">Sign up</a>
              <?php 
                if(isset($_GET['err'])){
                  echo '<p class="text-danger">Error logging in. Please try again.</p>';
                }
              ?>
            </form>
            <?php 
                }
                else {
                  echo '<ul class = "nav navbar-nav"><li><a href="/?profile&user='.$_SESSION['username'].'">'.$_SESSION['username'].'</a></li><li><a href="/?lgt">Log out</a></li></ul>';
                }
            ?>
          </div>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
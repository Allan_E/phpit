		<div id="content">
			<?php 
            if(!isset($_SESSION['username'])) {
                ?>
                    <div class = "well">
                        <h2 class = "lead">
                            Please login to submit a link.
                        </h2>
                    </div>
                <?php 
            }
            else {
				if(!isset($_POST['title'])){
			?>
                <div class = "well">
                    <form role = "form" action="" method="post">
                        <div class = "form-group">
                            <label for="title" class="control-label">Title: </label>
                            <input type="text" class = "form-control" placeholder = "Enter a title..." name="title" maxlength="255"/>
                        </div>
                        <div class = "form-group">
                            <label for="link" class="control-label">Link: </label>
                            <input type="text" class = "form-control" placeholder = "Enter a (valid) link..." name="link" maxlength="255"/>
                        </div>
                        <div class = "form-group">
                            <input type="submit" text="submit" class = "btn btn-primary"/>
                        </div>
                    </form>
                </div>
			<?php
				}
				else {                   
					$titleunsanitized = $_POST['title'];
					$linkunsanitized = $_POST['link'];
									
					$title = htmlspecialchars($titleunsanitized, ENT_QUOTES );
					$link = htmlspecialchars($linkunsanitized, ENT_QUOTES );
					$date = htmlspecialchars(date('jS \of F Y h:i:s A'), ENT_QUOTES );
                    
                    if(empty($title)){
                    	echo '<p class = "lead">ERROR: Please enter a title</p>';
                    	?>
                           <div class = "well">
                                <form role = "form" action="" method="post">
                                    <div class = "form-group has-error">
                                        <label for="title" class="control-label">Title: </label>
                                        <input type="text" class = "form-control" id = "inputError" placeholder = "Enter a title..." name="title" maxlength="255"/>
                                    </div>
                                    <div class = "form-group">
                                        <label for="link" class="control-label">Link: </label>
                                        <input type="text" class = "form-control" placeholder = "Enter a (valid) link..." name="link" maxlength="255"/>
                                    </div>
                                    <div class = "form-group">
                                        <input type="submit" text="submit" class = "btn btn-primary"/>
                                    </div>
                                </form>
                            </div>                   
                    	<?php
                    }
                    else if(empty($link)){
                    	echo '<p class = "lead">ERROR: Please enter a link</p>';
                        ?>
                            <div class = "well">
                                <form role = "form" action="" method="post">
                                    <div class = "form-group">
                                        <label for="title" class="control-label">Title: </label>
                                        <input type="text" class = "form-control" placeholder = "Enter a title..." name="title" maxlength="255"/>
                                    </div>
                                    <div class = "form-group has-error">
                                        <label for="link" class="control-label">Link: </label>
                                        <input type="text" class = "form-control" id = "inputError" placeholder = "Enter a (valid) link..." name="link" maxlength="255"/>
                                    </div>
                                    <div class = "form-group">
                                        <input type="submit" text="submit" class = "btn btn-primary"/>
                                    </div>
                                </form>
                            </div>
                    	<?php
                    }    
                    else if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $link)){
                        echo '<p class = "lead">ERROR: Please enter a valid link</p>';
                        ?>
                            <div class = "well">
                                <form role = "form" action="" method="post">
                                    <div class = "form-group">
                                        <label for="title" class="control-label">Title: </label>
                                        <input type="text" class = "form-control" placeholder = "Enter a title..." name="title" maxlength="255"/>
                                    </div>
                                    <div class = "form-group has-error">
                                        <label for="link" class="control-label">Link: </label>
                                        <input type="text" class = "form-control" id = "inputError" placeholder = "Enter a (valid) link..." name="link" maxlength="255"/>
                                    </div>
                                    <div class = "form-group">
                                        <input type="submit" text="submit" class = "btn btn-primary"/>
                                    </div>
                                </form>
                            </div>
                    	<?php
                    }     
                    else {
                        $user = $_SESSION['username'];
						$q = "INSERT INTO links (id, title, link, date, votecount, author) VALUES ('NULL', '$title', '$link', '$date', '1', '$user')";
						mysql_query($q) or die(mysql_error());

						echo '<p>success!</p>';
                    	echo '<p><span class="return-link"><a href="/">Return</a></span></p>';
					}
				}
            }
			?>
		</div>
